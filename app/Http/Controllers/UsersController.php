<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function create(){
        return view('layout.sigin');
    }
    public function table(Request $request){
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'nohp' => 'required',
                'alamat' => 'required',
            ],
            [
                'nam.required' => 'Nama harus diisi',
                'email.required'  => 'Email harus diisi',
                'password.required'  => 'Password harus diisi',
                'nohp.required'  => 'Password harus diisi',
                'alamat.required'  => 'Password harus diisi',
            ]
        );

        DB::table('users')->insert(
            [
                'name' => $request ['name'], 
                'email' => $request ['email'],
                'password' => $request ['password'],
            ]
        );
        DB::table('profile')->insert(
            [
                'telephone' => $request ['nohp'], 
                'alamat' => $request ['alamat'],
            ]
        );
        return redirect('/');
    }
    public function view(){
        $users = DB::table('users')->get();
        return view('layout.tableusers', compact('users'));
    }
    public function login(){
        return view('layout.login');
    }
    
}
