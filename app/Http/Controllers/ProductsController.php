<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function tambah_barang()
    {
        return view('layout.tambah_barang');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_barang' => 'required',
            'file' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi' =>'required',
            
        ],
        [
            'nama_barang.required' => 'Inputan Nama Barang Harus Diisi!',
            'file.required' => 'Inputan Gambar Barang Harus Diisi!',
            'harga.required' => 'Inputan Harga Barang Harus Diisi!',
            'stok.required' => 'Inputan Stock Barang Harus Diisi!',
            
        ]
    
    );

    DB::table('products')->insert(
        ['nama_barang' => $request['nama_barang'],
         'gambar' => $request['gambar'],
         'harga' => $request['harga'],
         'stok' => $request['stok'],
         'deskripsi' => $request['deskripsi'],
         
        ]
    );
        return redirect('/layout/tambah_barang');
    }
}
