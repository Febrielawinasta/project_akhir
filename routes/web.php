<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProductsController;
use Illuminate\Support\Facades\Route;


Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/', function () {
    return view('layout.home');
});

Route::get('/login', 'UsersController@login');

Route::get('/sigin', 'UsersController@create');

Route::get('/data_users', 'UsersController@view');

Route::post('/data_users', 'UsersController@table');

//jenis tanaman
Route::get('/vegetable', function(){
    return view('barang.vegetable');
});

//link pembelian

Route::get('/purchase', function(){
    return view('layout.purchase');
});


Route::get('/tambah_barang', 'ProductsController@tambah_barang');

Route::post('/products','ProductsController@store');

