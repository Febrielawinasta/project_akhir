@extends('layout.master')
@section('load')
@endsection
@section('users')
    Sign In
@endsection
@section('lusers')
<a href="/sigin"><i class="fa fa-user"></i> Sigin</a>
@endsection
@section('isi')
<div class="container ">
    <div class="p-5">
        <form action="/home" method="post"  class="m-5">
            <div class="hero ml-4 fw-bold">
                <h2>Log In</h2>
            </div>
            @csrf
            <div>
                <label for="email" class="col-sm-2 col-form-label">Email</label>
                <input type="text" name="email" id="email" class="col-sm-5" required><br>
            <br>
            </div>
            <div>
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <input type="password" name="password" id="password" class="col-sm-5" required><br>
            <br>
            </div>
            <div class="">
                <label class="col-form-label col-sm-5"></label>
                <a href="/login" class="btn btn-success col-sm-2 text-end">Log In</a>
            <br>
            </div>
            
    </div>
        
</div>
@endsection