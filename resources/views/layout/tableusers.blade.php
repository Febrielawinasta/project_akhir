@extends('layout.master')
@section('load')
@endsection
@section('users')
    Sign In
@endsection
@section('isi')

<a href="/cast/create" class="btn btn-primary">Tambah Data</a>
<br>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Email</th>
        <th scope="col">Password</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($users as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->email}}</td>
              <td>{{$item->password}}</td>
              <td>
                  
                  <form action="cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a class="btn btn-info btn-sm" href="/cast/{{$item->id}}">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                  </form>
              </td>
          </tr>
      @empty
           <tr>
               <td>Belum Ada Data</td>
           </tr>
      @endforelse
    </tbody>
  </table>

@endsection