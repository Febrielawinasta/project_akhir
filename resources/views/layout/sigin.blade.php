@extends('layout.master')
@section('load')
@endsection
@section('users')
    Login
@endsection
@section('lusers')
<a href="/login"><i class="fa fa-user"></i> Sigin</a>
@endsection
@section('isi')

<section class="hero">
    
    <div class="container">
        <div class="ml-5">
            <div class="hero m-4 fw-bold">
                    <h2>Sign Up Form</h2>
            </div>
            <form action="/data_users" method="POST">
                @csrf
                <div>
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <input type="text" name="name" id="name" class="col-sm-5" required><br>
                <br>
                </div>
                
                <div>
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <input type="text" name="email" id="email" class="col-sm-5" required><br>
                <br>
                </div>
                <div>
                    <label for="email_verified" class="col-sm-2 col-form-label">Konfirmasi</label>
                    <input type="text" name="email_verified" id="email_verified" class="col-sm-3" required><br>
                <br>
                </div>
                <div>
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <input type="password" name="password" id="password" class="col-sm-3" required><br>
                <br>
                </div>
                <div>
                    <label for="nohp" class="col-sm-2 col-form-label">No. Tlp.</label>
                    <input type="text" name="nohp" id="nohp" class="col-sm-5" required><br>
                <br>
                </div>
                <div>
                    <label for="gender" class="col-sm-2 col-form-label">Gender</label>
                    <input type="radio" name="gender" value="male" required>
                    <label for="gender ">Male</label>
                    <input type="radio" name="gender" value="female" class="ml-5"required>
                    <label for="gender">Female</label>
                <br>
                </div>
                <div>
                    <br>
                    <label for="alamat" class="col-sm-2 col-form-label align-top">Alamat</label>
                    <textarea name="alamat" id="" cols="60" rows="10" required></textarea><br>
                <br>
                </div>
                <div>
                    <label class="col-sm-2 col-form-label align-top"></label>
                    <input type="submit" value="Sign Up" class=" col-sm-2 btn btn-success mr-2">
                    <a href="/login" class="btn btn-outline-primary col-sm-2">Log In</a>
                <br>
                </div>
            </form>    
        </div>     
    </div>
</section>
<!-- Hero Section End -->




@endsection