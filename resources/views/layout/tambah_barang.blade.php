@extends('layout.master')
@section('load')
@endsection

@section('isi')
<div class="container">
    <div class="p-5">
        <form action="/products" method="POST"  class="m-5">
            <div class="hero ml-4 fw-bold">
                <h2>Tambah Barang</h2>
            </div>
            @csrf
            <div>
                <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                <input type="text" name="nama_barang" id="nama_barang" class="col-sm-5" required><br>
            <br>
            </div>
            @error('nama_barang')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div>
                <label for="gambar" class="col-sm-2 col-form-control-file">Gambar Barang</label>
                <input type="file" name="gambar" id="gambar" class="col-sm-5" required><br>
            <br>
            </div>
            <div>
                <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                <input type="text" name="harga" id="harga" class="col-sm-5" required><br>
            <br>
            </div>
            @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div>
                <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                <input type="text" name="stok" id="stok" class="col-sm-5" required><br>
            <br>
            </div>
            @error('stok')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div>
                <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                <textarea name="deskripsi"cols="30" rows="5"></textarea>
            </div>
            <div>
                <label class="col-sm-2 col-form-label align-top"></label>
                <button type="submit" class="btn btn-primary"> Tambah </button>
            <br>
            </div>
            
    </div>
        
</div>
@endsection